from abc import ABC, abstractmethod

# 1. Create a Person class
class Person(ABC):
	@abstractmethod
	def getFullName(self):
		pass

	@abstractmethod
	def addRequest(self):
		pass

	@abstractmethod
	def checkRequest(self):
		pass

	@abstractmethod
	def addUser(self):
		pass


# 2. Create an Employee class
class Employee(Person):
  #properties
	def __init__(self, firstName, lastName, email, department):
		self._firstName = firstName
		self._lastName = lastName
		self._email = email
		self._department = department

  #setters
	def set_firstName(self, firstName):
		self._firstName = firstName

	def set_lastName(self, lastName):
		self._lastName = lastName
		
	def set_email(self, email):
		self._email = email
		
	def set_department(self, department):
		self._department = department

  #getters
	def get_firstName(self):
		return self._firstName

	def get_lastName(self):
		return self._lastName
		
	def get_email(self):
		return self._email
		
	def get_department(self):
		return self._department
		
  #abstractmethods
	def checkRequest(self):
		pass

	def addUser(self):
		pass

	def addRequest(self):
		return "Request has been added"

	def getFullName(self):
		return self._firstName + " " + self._lastName

  #customMethods
	def login(self):
		return self._email + " has logged in"

	def logout(self):
		return self._email + " has logged out"


# 3. Create a TeamLead class
class TeamLead(Person):
  #properties
	def __init__(self, firstName, lastName, email, department):
		self._firstName = firstName
		self._lastName = lastName
		self._email = email
		self._department = department
		self._members = []

  #setters
	def set_firstName(self, firstName):
		self._firstName = firstName

	def set_lastName(self, lastName):
		self._lastName = lastName
		
	def set_email(self, email):
		self._email = email
		
	def set_department(self, department):
		self._department = department

	# def set_members(self, members):
	# 	self._members = members

  #getters
	def get_firstName(self):
		return self._firstName

	def get_lastName(self):
		return self._lastName
		
	def get_email(self):
		return self._email
		
	def get_department(self):
		return self._department

	def get_members(self):
		return self._members

  #abstractmethods
	def addRequest(self):
		pass

	def addUser(self):
		pass

	def checkRequest(self):
		return "Request has been checked"

	def getFullName(self):
		return self._firstName + " " + self._lastName

  #customMethods
	def login(self):
		return self._email + " has logged in"

	def logout(self):
		return self._email + " has logged out"

	def addMember(self, member):
		self._members.append(member)


# 4. Create an Admin class
class Admin(Person):
  #properties
	def __init__(self, firstName, lastName, email, department):
		self._firstName = firstName
		self._lastName = lastName
		self._email = email
		self._department = department

  #setters
	def set_firstName(self, firstName):
		self._firstName = firstName

	def set_lastName(self, lastName):
		self._lastName = lastName
		
	def set_email(self, email):
		self._email = email
		
	def set_department(self, department):
		self._department = department

  #getters
	def get_firstName(self):
		return self._firstName

	def get_lastName(self):
		return self._lastName
		
	def get_email(self):
		return self._email
		
	def get_department(self):
		return self._department

  #abstractmethods
	def checkRequest(self):
		# pass
		return "Request has been checked"

	def addRequest(self):
		# pass
		return "Request has been added"

	def addUser(self):
		# pass
		return "User has been added"

	def getFullName(self):
		return self._firstName + " " + self._lastName

  #customMethods
	def login(self):
		return self._email + " has logged in"

	def logout(self):
		return self._email + " has logged out"


# 5. Create a Request Class
class Request():
  #properties
	def __init__(self, name, requester, dateRequested):
		self._name = name
		self._requester = requester
		self._dateRequested = dateRequested

		self._status = "Open"

  #setters
	def set_name(self, name):
		self._name = name

	def set_requester(self, requester):
		self._requester = requester

	def set_dateRequested(self, dateRequested):
		self._dateRequested = dateRequested

	def set_status(self, status):
		self._status = status

  #getters
	def get_name(self):
		return self._name

	def get_requester(self):
		return self._requester

	def get_dateRequested(self):
		return self._dateRequested

	def get_status(self):
		return self._status
  	
  #methods
	def updateRequest(self):
		return "Request " + self._name + " has been updated"

	def closeRequest(self):
		return "Request " + self._name + " has been closed"

	def cancelRequest(self):
		return "Request " + self._name + " has been cancelled"


# Test cases:
employee1 = Employee("John", "Doe", "djohn@mail.com", "Marketing")
employee2 = Employee("Jane", "Smith", "sjane@mail.com", "Marketing")
employee3 = Employee("Robert", "Patterson", "probert@mail.com", "Sales")
employee4 = Employee("Brandon", "Smith", "sbrandon@mail.com", "Sales")
admin1 = Admin("Monika", "Justin", "jmonika@mail.com", "Marketing")
teamLead1 = TeamLead("Michael", "Specter", "smichael@mail.com", "Sales")
req1 = Request("New hire orientation", teamLead1, "27-Jul-2021")
req2 = Request("Laptop repair", employee1, "1-Jul-2021")

assert employee1.getFullName() == "John Doe", "Full name should be John Doe"
assert admin1.getFullName() == "Monika Justin", "Full name should be Monika Justin"
assert teamLead1.getFullName() == "Michael Specter", "Full name should be Michael Specter"
assert employee2.login() == "sjane@mail.com has logged in"
assert employee2.addRequest() == "Request has been added"
assert employee2.logout() == "sjane@mail.com has logged out"

teamLead1.addMember(employee3)
teamLead1.addMember(employee4)
for indiv_emp in teamLead1.get_members():
    print(indiv_emp.getFullName())

assert admin1.addUser() == "User has been added"

req2.set_status("closed")
print(req2.closeRequest())